﻿namespace Excel_VDO
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.btnAbout = new System.Windows.Forms.Button();
            this.btnOpen = new System.Windows.Forms.Button();
            this.txtPathExcel = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.label2 = new System.Windows.Forms.Label();
            this.txtBaseInfo = new System.Windows.Forms.TextBox();
            this.btnLoad = new System.Windows.Forms.Button();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox_IIN = new System.Windows.Forms.GroupBox();
            this.lbl_DateBirth = new System.Windows.Forms.Label();
            this.lbl_FIO = new System.Windows.Forms.Label();
            this.comboBox_FIOrow = new System.Windows.Forms.ComboBox();
            this.comboBox_FIOcol = new System.Windows.Forms.ComboBox();
            this.comboBox_row2 = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.radioButton_dateBirth = new System.Windows.Forms.RadioButton();
            this.comboBox_column2 = new System.Windows.Forms.ComboBox();
            this.radioButton_IIN = new System.Windows.Forms.RadioButton();
            this.btnSaveToExcel = new System.Windows.Forms.Button();
            this.btnCheck = new System.Windows.Forms.Button();
            this.lblCount = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.comboBoxIIN2 = new System.Windows.Forms.ComboBox();
            this.comboBoxIIN = new System.Windows.Forms.ComboBox();
            this.comboBoxDateBirth2 = new System.Windows.Forms.ComboBox();
            this.comboBoxDateBirth = new System.Windows.Forms.ComboBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.checkIIN = new System.Windows.Forms.CheckBox();
            this.checkBirth = new System.Windows.Forms.CheckBox();
            this.checkFIO = new System.Windows.Forms.CheckBox();
            this.btnHelp = new System.Windows.Forms.Button();
            this.btnLoadBase = new System.Windows.Forms.Button();
            this.comboBoxFIO2 = new System.Windows.Forms.ComboBox();
            this.lblRow = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblColumn = new System.Windows.Forms.Label();
            this.btnOpenBase = new System.Windows.Forms.Button();
            this.comboBoxFIO = new System.Windows.Forms.ComboBox();
            this.txtPathBase = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.groupBox_IIN.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnAbout
            // 
            this.btnAbout.Location = new System.Drawing.Point(9, 289);
            this.btnAbout.Name = "btnAbout";
            this.btnAbout.Size = new System.Drawing.Size(85, 32);
            this.btnAbout.TabIndex = 0;
            this.btnAbout.Text = "About";
            this.btnAbout.UseVisualStyleBackColor = true;
            this.btnAbout.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnOpen
            // 
            this.btnOpen.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnOpen.Location = new System.Drawing.Point(280, 45);
            this.btnOpen.Name = "btnOpen";
            this.btnOpen.Size = new System.Drawing.Size(78, 21);
            this.btnOpen.TabIndex = 1;
            this.btnOpen.Text = "Открыть";
            this.btnOpen.UseVisualStyleBackColor = true;
            this.btnOpen.Click += new System.EventHandler(this.btnOpen_Click);
            // 
            // txtPathExcel
            // 
            this.txtPathExcel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtPathExcel.Location = new System.Drawing.Point(10, 45);
            this.txtPathExcel.Name = "txtPathExcel";
            this.txtPathExcel.ReadOnly = true;
            this.txtPathExcel.Size = new System.Drawing.Size(264, 20);
            this.txtPathExcel.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(7, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 16);
            this.label1.TabIndex = 3;
            this.label1.Text = "Путь:";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(279, 174);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 16);
            this.label2.TabIndex = 5;
            this.label2.Text = "label2";
            this.label2.Visible = false;
            // 
            // txtBaseInfo
            // 
            this.txtBaseInfo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBaseInfo.Location = new System.Drawing.Point(160, 247);
            this.txtBaseInfo.Multiline = true;
            this.txtBaseInfo.Name = "txtBaseInfo";
            this.txtBaseInfo.Size = new System.Drawing.Size(199, 74);
            this.txtBaseInfo.TabIndex = 6;
            // 
            // btnLoad
            // 
            this.btnLoad.Location = new System.Drawing.Point(10, 209);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(145, 32);
            this.btnLoad.TabIndex = 7;
            this.btnLoad.Text = "Загрузить";
            this.btnLoad.UseVisualStyleBackColor = true;
            this.btnLoad.Click += new System.EventHandler(this.button3_Click);
            // 
            // listBox1
            // 
            this.listBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(177, 77);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(181, 225);
            this.listBox1.TabIndex = 8;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.groupBox_IIN);
            this.groupBox1.Controls.Add(this.btnSaveToExcel);
            this.groupBox1.Controls.Add(this.btnCheck);
            this.groupBox1.Controls.Add(this.lblCount);
            this.groupBox1.Controls.Add(this.listBox1);
            this.groupBox1.Controls.Add(this.btnLoad);
            this.groupBox1.Controls.Add(this.btnOpen);
            this.groupBox1.Controls.Add(this.txtPathExcel);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox1.Location = new System.Drawing.Point(383, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(367, 334);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Проверяемый файл";
            // 
            // groupBox_IIN
            // 
            this.groupBox_IIN.Controls.Add(this.lbl_DateBirth);
            this.groupBox_IIN.Controls.Add(this.lbl_FIO);
            this.groupBox_IIN.Controls.Add(this.comboBox_FIOrow);
            this.groupBox_IIN.Controls.Add(this.comboBox_FIOcol);
            this.groupBox_IIN.Controls.Add(this.comboBox_row2);
            this.groupBox_IIN.Controls.Add(this.label5);
            this.groupBox_IIN.Controls.Add(this.label4);
            this.groupBox_IIN.Controls.Add(this.radioButton_dateBirth);
            this.groupBox_IIN.Controls.Add(this.comboBox_column2);
            this.groupBox_IIN.Controls.Add(this.radioButton_IIN);
            this.groupBox_IIN.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox_IIN.Location = new System.Drawing.Point(10, 77);
            this.groupBox_IIN.Name = "groupBox_IIN";
            this.groupBox_IIN.Size = new System.Drawing.Size(161, 126);
            this.groupBox_IIN.TabIndex = 16;
            this.groupBox_IIN.TabStop = false;
            this.groupBox_IIN.Text = "Параметры поиска:";
            // 
            // lbl_DateBirth
            // 
            this.lbl_DateBirth.AutoSize = true;
            this.lbl_DateBirth.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbl_DateBirth.Location = new System.Drawing.Point(116, 56);
            this.lbl_DateBirth.Name = "lbl_DateBirth";
            this.lbl_DateBirth.Size = new System.Drawing.Size(30, 13);
            this.lbl_DateBirth.TabIndex = 22;
            this.lbl_DateBirth.Text = "Д/р:";
            // 
            // lbl_FIO
            // 
            this.lbl_FIO.AutoSize = true;
            this.lbl_FIO.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbl_FIO.Location = new System.Drawing.Point(64, 56);
            this.lbl_FIO.Name = "lbl_FIO";
            this.lbl_FIO.Size = new System.Drawing.Size(37, 13);
            this.lbl_FIO.TabIndex = 21;
            this.lbl_FIO.Text = "ФИО:";
            // 
            // comboBox_FIOrow
            // 
            this.comboBox_FIOrow.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_FIOrow.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.comboBox_FIOrow.FormattingEnabled = true;
            this.comboBox_FIOrow.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20"});
            this.comboBox_FIOrow.Location = new System.Drawing.Point(119, 99);
            this.comboBox_FIOrow.Name = "comboBox_FIOrow";
            this.comboBox_FIOrow.Size = new System.Drawing.Size(36, 21);
            this.comboBox_FIOrow.TabIndex = 20;
            // 
            // comboBox_FIOcol
            // 
            this.comboBox_FIOcol.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_FIOcol.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.comboBox_FIOcol.FormattingEnabled = true;
            this.comboBox_FIOcol.Items.AddRange(new object[] {
            "A",
            "B",
            "C",
            "D",
            "E",
            "F"});
            this.comboBox_FIOcol.Location = new System.Drawing.Point(119, 72);
            this.comboBox_FIOcol.Name = "comboBox_FIOcol";
            this.comboBox_FIOcol.Size = new System.Drawing.Size(36, 21);
            this.comboBox_FIOcol.TabIndex = 19;
            // 
            // comboBox_row2
            // 
            this.comboBox_row2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_row2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.comboBox_row2.FormattingEnabled = true;
            this.comboBox_row2.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20"});
            this.comboBox_row2.Location = new System.Drawing.Point(63, 99);
            this.comboBox_row2.Name = "comboBox_row2";
            this.comboBox_row2.Size = new System.Drawing.Size(36, 21);
            this.comboBox_row2.TabIndex = 18;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(9, 102);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(46, 13);
            this.label5.TabIndex = 17;
            this.label5.Text = "Строка:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(5, 76);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 13);
            this.label4.TabIndex = 18;
            this.label4.Text = "Столбец:";
            // 
            // radioButton_dateBirth
            // 
            this.radioButton_dateBirth.AutoSize = true;
            this.radioButton_dateBirth.Checked = true;
            this.radioButton_dateBirth.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.radioButton_dateBirth.Location = new System.Drawing.Point(6, 19);
            this.radioButton_dateBirth.Name = "radioButton_dateBirth";
            this.radioButton_dateBirth.Size = new System.Drawing.Size(118, 17);
            this.radioButton_dateBirth.TabIndex = 1;
            this.radioButton_dateBirth.TabStop = true;
            this.radioButton_dateBirth.Text = "По дате рождения";
            this.radioButton_dateBirth.UseVisualStyleBackColor = true;
            this.radioButton_dateBirth.CheckedChanged += new System.EventHandler(this.radioButton_dateBirth_CheckedChanged);
            // 
            // comboBox_column2
            // 
            this.comboBox_column2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_column2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.comboBox_column2.FormattingEnabled = true;
            this.comboBox_column2.Items.AddRange(new object[] {
            "A",
            "B",
            "C",
            "D",
            "E",
            "F"});
            this.comboBox_column2.Location = new System.Drawing.Point(63, 73);
            this.comboBox_column2.Name = "comboBox_column2";
            this.comboBox_column2.Size = new System.Drawing.Size(36, 21);
            this.comboBox_column2.TabIndex = 17;
            // 
            // radioButton_IIN
            // 
            this.radioButton_IIN.AutoSize = true;
            this.radioButton_IIN.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.radioButton_IIN.Location = new System.Drawing.Point(6, 36);
            this.radioButton_IIN.Name = "radioButton_IIN";
            this.radioButton_IIN.Size = new System.Drawing.Size(66, 17);
            this.radioButton_IIN.TabIndex = 0;
            this.radioButton_IIN.Text = "По ИИН";
            this.radioButton_IIN.UseVisualStyleBackColor = true;
            this.radioButton_IIN.CheckedChanged += new System.EventHandler(this.radioButton_IIN_CheckedChanged);
            // 
            // btnSaveToExcel
            // 
            this.btnSaveToExcel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnSaveToExcel.Location = new System.Drawing.Point(20, 286);
            this.btnSaveToExcel.Name = "btnSaveToExcel";
            this.btnSaveToExcel.Size = new System.Drawing.Size(124, 38);
            this.btnSaveToExcel.TabIndex = 11;
            this.btnSaveToExcel.Text = "Открыть результаты в Excel";
            this.btnSaveToExcel.UseVisualStyleBackColor = true;
            this.btnSaveToExcel.Click += new System.EventHandler(this.btnSaveToExcel_Click);
            // 
            // btnCheck
            // 
            this.btnCheck.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnCheck.Location = new System.Drawing.Point(10, 247);
            this.btnCheck.Name = "btnCheck";
            this.btnCheck.Size = new System.Drawing.Size(145, 33);
            this.btnCheck.TabIndex = 10;
            this.btnCheck.Text = "Начать проверку";
            this.btnCheck.UseVisualStyleBackColor = true;
            this.btnCheck.Click += new System.EventHandler(this.btnCheck_Click);
            // 
            // lblCount
            // 
            this.lblCount.AutoSize = true;
            this.lblCount.Location = new System.Drawing.Point(174, 308);
            this.lblCount.Name = "lblCount";
            this.lblCount.Size = new System.Drawing.Size(67, 16);
            this.lblCount.TabIndex = 9;
            this.lblCount.Text = "Всего: 0";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.comboBoxIIN2);
            this.groupBox2.Controls.Add(this.comboBoxIIN);
            this.groupBox2.Controls.Add(this.comboBoxDateBirth2);
            this.groupBox2.Controls.Add(this.comboBoxDateBirth);
            this.groupBox2.Controls.Add(this.groupBox3);
            this.groupBox2.Controls.Add(this.btnHelp);
            this.groupBox2.Controls.Add(this.btnLoadBase);
            this.groupBox2.Controls.Add(this.comboBoxFIO2);
            this.groupBox2.Controls.Add(this.lblRow);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.lblColumn);
            this.groupBox2.Controls.Add(this.btnOpenBase);
            this.groupBox2.Controls.Add(this.comboBoxFIO);
            this.groupBox2.Controls.Add(this.txtPathBase);
            this.groupBox2.Controls.Add(this.txtBaseInfo);
            this.groupBox2.Controls.Add(this.btnAbout);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox2.Location = new System.Drawing.Point(12, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(365, 334);
            this.groupBox2.TabIndex = 10;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Файл с общей базой";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label8.Location = new System.Drawing.Point(200, 176);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(34, 13);
            this.label8.TabIndex = 26;
            this.label8.Text = "ИИН:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.Location = new System.Drawing.Point(126, 176);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(68, 13);
            this.label7.TabIndex = 25;
            this.label7.Text = "Дата рожд.:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(68, 176);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(37, 13);
            this.label6.TabIndex = 24;
            this.label6.Text = "ФИО:";
            // 
            // comboBoxIIN2
            // 
            this.comboBoxIIN2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxIIN2.FormattingEnabled = true;
            this.comboBoxIIN2.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20"});
            this.comboBoxIIN2.Location = new System.Drawing.Point(203, 217);
            this.comboBoxIIN2.Name = "comboBoxIIN2";
            this.comboBoxIIN2.Size = new System.Drawing.Size(48, 24);
            this.comboBoxIIN2.TabIndex = 23;
            this.comboBoxIIN2.Visible = false;
            // 
            // comboBoxIIN
            // 
            this.comboBoxIIN.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxIIN.FormattingEnabled = true;
            this.comboBoxIIN.Items.AddRange(new object[] {
            "A",
            "B",
            "C",
            "D",
            "E",
            "F"});
            this.comboBoxIIN.Location = new System.Drawing.Point(203, 192);
            this.comboBoxIIN.Name = "comboBoxIIN";
            this.comboBoxIIN.Size = new System.Drawing.Size(48, 24);
            this.comboBoxIIN.TabIndex = 22;
            this.comboBoxIIN.Visible = false;
            // 
            // comboBoxDateBirth2
            // 
            this.comboBoxDateBirth2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxDateBirth2.FormattingEnabled = true;
            this.comboBoxDateBirth2.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20"});
            this.comboBoxDateBirth2.Location = new System.Drawing.Point(138, 217);
            this.comboBoxDateBirth2.Name = "comboBoxDateBirth2";
            this.comboBoxDateBirth2.Size = new System.Drawing.Size(48, 24);
            this.comboBoxDateBirth2.TabIndex = 21;
            this.comboBoxDateBirth2.Visible = false;
            // 
            // comboBoxDateBirth
            // 
            this.comboBoxDateBirth.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxDateBirth.FormattingEnabled = true;
            this.comboBoxDateBirth.Items.AddRange(new object[] {
            "A",
            "B",
            "C",
            "D",
            "E",
            "F"});
            this.comboBoxDateBirth.Location = new System.Drawing.Point(138, 192);
            this.comboBoxDateBirth.Name = "comboBoxDateBirth";
            this.comboBoxDateBirth.Size = new System.Drawing.Size(48, 24);
            this.comboBoxDateBirth.TabIndex = 20;
            this.comboBoxDateBirth.Visible = false;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.checkIIN);
            this.groupBox3.Controls.Add(this.checkBirth);
            this.groupBox3.Controls.Add(this.checkFIO);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox3.Location = new System.Drawing.Point(9, 77);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(161, 86);
            this.groupBox3.TabIndex = 19;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Параметры загрузки:";
            // 
            // checkIIN
            // 
            this.checkIIN.AutoSize = true;
            this.checkIIN.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkIIN.Location = new System.Drawing.Point(6, 62);
            this.checkIIN.Name = "checkIIN";
            this.checkIIN.Size = new System.Drawing.Size(50, 17);
            this.checkIIN.TabIndex = 21;
            this.checkIIN.Text = "ИИН";
            this.checkIIN.UseVisualStyleBackColor = true;
            this.checkIIN.CheckedChanged += new System.EventHandler(this.checkIIN_CheckedChanged);
            // 
            // checkBirth
            // 
            this.checkBirth.AutoSize = true;
            this.checkBirth.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkBirth.Location = new System.Drawing.Point(6, 42);
            this.checkBirth.Name = "checkBirth";
            this.checkBirth.Size = new System.Drawing.Size(105, 17);
            this.checkBirth.TabIndex = 20;
            this.checkBirth.Text = "Дата рождения";
            this.checkBirth.UseVisualStyleBackColor = true;
            this.checkBirth.CheckedChanged += new System.EventHandler(this.checkBirth_CheckedChanged);
            // 
            // checkFIO
            // 
            this.checkFIO.AutoSize = true;
            this.checkFIO.Checked = true;
            this.checkFIO.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkFIO.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkFIO.Location = new System.Drawing.Point(6, 22);
            this.checkFIO.Name = "checkFIO";
            this.checkFIO.Size = new System.Drawing.Size(53, 17);
            this.checkFIO.TabIndex = 19;
            this.checkFIO.Text = "ФИО";
            this.checkFIO.UseVisualStyleBackColor = true;
            this.checkFIO.CheckedChanged += new System.EventHandler(this.checkFIO_CheckedChanged);
            // 
            // btnHelp
            // 
            this.btnHelp.Location = new System.Drawing.Point(100, 289);
            this.btnHelp.Name = "btnHelp";
            this.btnHelp.Size = new System.Drawing.Size(51, 32);
            this.btnHelp.TabIndex = 10;
            this.btnHelp.Text = "?";
            this.btnHelp.UseVisualStyleBackColor = true;
            this.btnHelp.Click += new System.EventHandler(this.button2_Click);
            // 
            // btnLoadBase
            // 
            this.btnLoadBase.Location = new System.Drawing.Point(9, 247);
            this.btnLoadBase.Name = "btnLoadBase";
            this.btnLoadBase.Size = new System.Drawing.Size(142, 36);
            this.btnLoadBase.TabIndex = 9;
            this.btnLoadBase.Text = "Загрузить базу";
            this.btnLoadBase.UseVisualStyleBackColor = true;
            this.btnLoadBase.Click += new System.EventHandler(this.btnLoadBase_Click);
            // 
            // comboBoxFIO2
            // 
            this.comboBoxFIO2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxFIO2.FormattingEnabled = true;
            this.comboBoxFIO2.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20"});
            this.comboBoxFIO2.Location = new System.Drawing.Point(71, 217);
            this.comboBoxFIO2.Name = "comboBoxFIO2";
            this.comboBoxFIO2.Size = new System.Drawing.Size(48, 24);
            this.comboBoxFIO2.TabIndex = 15;
            // 
            // lblRow
            // 
            this.lblRow.AutoSize = true;
            this.lblRow.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblRow.Location = new System.Drawing.Point(13, 222);
            this.lblRow.Name = "lblRow";
            this.lblRow.Size = new System.Drawing.Size(53, 13);
            this.lblRow.TabIndex = 14;
            this.lblRow.Text = "Строка:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(6, 26);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(43, 16);
            this.label3.TabIndex = 9;
            this.label3.Text = "Путь:";
            // 
            // lblColumn
            // 
            this.lblColumn.AutoSize = true;
            this.lblColumn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblColumn.Location = new System.Drawing.Point(6, 197);
            this.lblColumn.Name = "lblColumn";
            this.lblColumn.Size = new System.Drawing.Size(60, 13);
            this.lblColumn.TabIndex = 13;
            this.lblColumn.Text = "Столбец:";
            // 
            // btnOpenBase
            // 
            this.btnOpenBase.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnOpenBase.Location = new System.Drawing.Point(273, 45);
            this.btnOpenBase.Name = "btnOpenBase";
            this.btnOpenBase.Size = new System.Drawing.Size(86, 21);
            this.btnOpenBase.TabIndex = 9;
            this.btnOpenBase.Text = "Открыть";
            this.btnOpenBase.UseVisualStyleBackColor = true;
            this.btnOpenBase.Click += new System.EventHandler(this.btnOpenBase_Click);
            // 
            // comboBoxFIO
            // 
            this.comboBoxFIO.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxFIO.FormattingEnabled = true;
            this.comboBoxFIO.Items.AddRange(new object[] {
            "A",
            "B",
            "C",
            "D",
            "E",
            "F"});
            this.comboBoxFIO.Location = new System.Drawing.Point(71, 192);
            this.comboBoxFIO.Name = "comboBoxFIO";
            this.comboBoxFIO.Size = new System.Drawing.Size(48, 24);
            this.comboBoxFIO.TabIndex = 12;
            // 
            // txtPathBase
            // 
            this.txtPathBase.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtPathBase.Location = new System.Drawing.Point(6, 46);
            this.txtPathBase.Name = "txtPathBase";
            this.txtPathBase.ReadOnly = true;
            this.txtPathBase.Size = new System.Drawing.Size(261, 20);
            this.txtPathBase.TabIndex = 9;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(760, 358);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Поиск участков в общей базе РПБ (Excel)";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox_IIN.ResumeLayout(false);
            this.groupBox_IIN.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnAbout;
        private System.Windows.Forms.Button btnOpen;
        private System.Windows.Forms.TextBox txtPathExcel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtBaseInfo;
        private System.Windows.Forms.Button btnLoad;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnOpenBase;
        private System.Windows.Forms.TextBox txtPathBase;
        private System.Windows.Forms.Button btnLoadBase;
        private System.Windows.Forms.Label lblCount;
        private System.Windows.Forms.Button btnCheck;
        private System.Windows.Forms.Button btnHelp;
        private System.Windows.Forms.Button btnSaveToExcel;
        private System.Windows.Forms.Label lblColumn;
        private System.Windows.Forms.ComboBox comboBoxFIO;
        private System.Windows.Forms.ComboBox comboBoxFIO2;
        private System.Windows.Forms.Label lblRow;
        private System.Windows.Forms.GroupBox groupBox_IIN;
        private System.Windows.Forms.RadioButton radioButton_dateBirth;
        private System.Windows.Forms.RadioButton radioButton_IIN;
        private System.Windows.Forms.ComboBox comboBox_row2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox comboBox_column2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.CheckBox checkIIN;
        private System.Windows.Forms.CheckBox checkBirth;
        private System.Windows.Forms.CheckBox checkFIO;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox comboBoxIIN2;
        private System.Windows.Forms.ComboBox comboBoxIIN;
        private System.Windows.Forms.ComboBox comboBoxDateBirth2;
        private System.Windows.Forms.ComboBox comboBoxDateBirth;
        private System.Windows.Forms.Label lbl_DateBirth;
        private System.Windows.Forms.Label lbl_FIO;
        private System.Windows.Forms.ComboBox comboBox_FIOrow;
        private System.Windows.Forms.ComboBox comboBox_FIOcol;
    }
}

