﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;

namespace Excel_VDO
{
    public partial class Form1 : Form
    {
        Excel.Application excelapp = new Excel.Application();
        Excel.Workbook workbook;
        Excel.Worksheet worksheet;
        
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show(@"Поиск совпадений по Excel-файлам.
Программа ищет точные совпадения записей в участке по общей базе РПБ.
*****************************
Версия 1.1
- В общей базе удаляет в конце пробелы, если они есть
- Добавлена иконка
*****************************
Автор: Нурбек Бексатов, май 2017 ©
ГКП на ПХВ ""Центр психического здоровья"" УЗ г.Алматы
email: pchelpkz16@gmail.com", "О программе");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            MessageBox.Show(@"Программа ищет совпадения записей в участках 
по общей базе, скачанной с портала РПБ (https://erpb.eisz.kz) в виде Excel-файла.
Файл с общей базой загружать не более 12000, а участок - не более 1000 записей.

1) В левой стороне программы 'Файл с общей базой' загрузите excel-файл с 
общей базой (*.xls или *.xlsx)
2) Выберите параметры загрузки: ФИО и/или Дату рождения и/или ИИН
3) Заполните столбец и строку с которых начинаются данные: ФИО, 
Дата рождения и т.д. (Например: столбец B, строка 5)
4) Нажмите 'Загрузить базу'. Программа начнет считывать данные 
с файла и загружать в базу. В это время программа может на несколько 
секунд зависнуть. Как только данные будут загружены, в поле появится 
сообщение: 'Данные загружены' и количество найденных записей
5) В левой стороне программы 'Проверяемый файл' выберите файл Excel (участок)
6) Выберите параметры поиска: По ИИН или по Дате рождения (учтите, что,
если вы загрузили в общую базу Дату рождения, то и здесь нужно выбрать 
Дату рождения
7) Нажмите 'Загрузить' и данные загрузятся в поле справа, Внизу появится 
количество найденных записей
8) Нажмите 'Начать проверку'. Выйдет сообщение о найденных записях
9) Кнопка 'Сохранить результаты в Excel' запускает Microsoft Excel c совпавшими и не совпавшими записями.

- В общем списке формат даты должен стоять 'Общий', а в участке - 'Дата'", 
"Справка");
        }

        private void btnOpen_Click(object sender, EventArgs e)
        {
            OpenFileDialog openExcel = new OpenFileDialog();
            openExcel.Filter = "Excel 97-2003 (*.xls)|*.xls|Excel 2007 (*.xlsx)|*.xlsx";        // Форматы для Excel-файлов

            if (openExcel.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    txtPathExcel.Text = openExcel.FileName;

                }
                catch (Exception ex)
                {
                    MessageBox.Show("Ошибка при открытии файла: " + ex.Message);
                }
            }
        }

        string[] mass;
        List<string> lst = new List<string>();

        private void LoadFileExcel(string path, Excel.Application app, Excel.Workbook xlBook, 
                                Excel.Worksheet sheet, string col, int row, string col2, int row2)
        {
            xlBook = app.Workbooks.Open(path, 0, true, 5, "", "", true,
                                                  Microsoft.Office.Interop.Excel.XlPlatform.xlWindows,
                                                  "\t",
                                                  false,
                                                  false,
                                                  0,
                                                  true,
                                                  1,
                                                  0);
            sheet = (Excel.Worksheet)xlBook.Worksheets.get_Item(1);
            sheet.Range[sheet.Cells[row2, col2], sheet.Cells[row2, col2]].NumberFormat = "Общий";

            var zero1 = "";
            int counter1 = 0;

            for (int zer = 0; zer < 12000; zer++)
            {
                zero1 = sheet.get_Range(col + (zer + row).ToString(),
                                              col + (zer + row).ToString()).Value2;
                counter1++;

                if (zero1 == null)                  // Если zero1 равен нулю, останавливаем цикл
                {
                    zer = 12000;
                }
            }

            mass = new string[counter1 - 1];
            massDate = new string[counter1 - 1];

            for (int i = 0; i < mass.Length; i++)
            {
                mass[i] = sheet.get_Range(
                                        col + (i + row).ToString(),
                                        col + (i + row).ToString()).Value2.ToString();
                

                massDate[i] = sheet.get_Range(              // Заносим дату рождения в массив
                                        col2 + (i + row2).ToString(),
                                        col2 + (i + row2).ToString()).Text;


                listBox1.Items.Add(mass[i]);
                lst.Add(mass[i]);
            }
        }

        string[] massDate;

        private void button3_Click(object sender, EventArgs e)
        {
            listBox1.Items.Clear();
            Excel.Application xlApp = new Excel.Application();
            Excel.Workbook xlWorkBook = xlApp.Workbooks.Add(System.Reflection.Missing.Value);
            Excel.Worksheet xlWorkSheet = xlWorkBook.Sheets[1];
            object misValue = System.Reflection.Missing.Value;

            if (txtPathExcel.Text != String.Empty)
            {
                int row2 = 0;
                string col2 = "";
                int rowFIO = 0;
                string colFIO = "";

                if (radioButton_dateBirth.Checked == true)
                {
                    if (comboBox_column2.Text != "")
                    {
                        col2 = comboBox_column2.Text;
                        colFIO = comboBox_FIOcol.Text;
                    }

                    if (comboBox_row2.Text != "")
                    {
                        row2 = Convert.ToInt32(comboBox_row2.Text);
                        rowFIO = Convert.ToInt32(comboBox_FIOrow.Text);
                    }
                }

                if (radioButton_IIN.Checked == true)
                {
                    if (comboBox_column2.Text != "")
                    {
                        col2 = comboBox_column2.Text;
                    }

                    if (comboBox_row2.Text != "")
                    {
                        row2 = Convert.ToInt32(comboBox_row2.Text);
                    }
                }

                if (comboBox_column2.Text == "" | comboBox_row2.Text == "")
                {
                    MessageBox.Show("Выберите столбец и строку", "Предупреждение");
                    return;
                }

                LoadFileExcel(txtPathExcel.Text, xlApp, xlWorkBook, xlWorkSheet, col2, row2, colFIO, rowFIO);

                xlApp.DisplayAlerts = false;                // Не сохраняет данные
                xlWorkBook.Close(true, misValue, misValue);
                xlApp.Quit();

                releaseObject(xlWorkSheet);
                releaseObject(xlWorkBook);
                releaseObject(xlApp);
            }
            else
            {
                MessageBox.Show("Выберите файл (участок)!");
            }
            lblCount.Text = "Всего: " + listBox1.Items.Count.ToString();
        }

        private void releaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
                MessageBox.Show("Unable to release the Object " + ex.ToString());
            }
            finally
            {
                GC.Collect();
            }
        }

        string[] massBase = new string[12000];
        string[] massBase2;
        List<string> lstBase = new List<string>();              // Массив для даты рождения

        private void btnLoadBase_Click(object sender, EventArgs e)
        {
            txtBaseInfo.Text = "";
            Excel.Application xlAppBase;
            Excel.Workbook xlWorkBookBase;
            Excel.Worksheet xlWorkSheetBase;
            object misValue = System.Reflection.Missing.Value;

            xlAppBase = new Excel.Application();
            if (txtPathBase.Text != String.Empty)
            {
                xlWorkBookBase = xlAppBase.Workbooks.Open(txtPathBase.Text, 0, true, 5, "", "", true,
                                                    Microsoft.Office.Interop.Excel.XlPlatform.xlWindows,
                                                    "\t",
                                                    false,
                                                    false,
                                                    0,
                                                    true,
                                                    1,
                                                    0);
                xlWorkSheetBase = (Excel.Worksheet)xlWorkBookBase.Worksheets.get_Item(1);       // Первая страница

                int row1 = 0;
                int rowBirth = 0;
                int rowIIN = 0;

                if (comboBoxFIO2.Text != "")
                {
                    row1 = Convert.ToInt32(comboBoxFIO2.Text);
                }
                if (comboBoxDateBirth2.Text != "")
                {
                    rowBirth = Convert.ToInt32(comboBoxDateBirth2.Text);
                }
                if (comboBoxIIN2.Text != "")
                {
                    rowIIN = Convert.ToInt32(comboBoxIIN2.Text);
                }

                var zero = "";
                int counterBase = 0;

                for (int zer = 0; zer < 12000; zer++)               // Максимально 12000, т.к. больше ставить 
                {                                                   // смысла на данный момент нет

                    zero = xlWorkSheetBase.get_Range(comboBoxFIO.Text + (zer + row1).ToString(),
                                                    comboBoxFIO.Text + (zer + row1).ToString()).Value2;
                    counterBase++;                                  // Счетчик, который в дальнейшем
                                                                    // определит длину массива
                    if (zero == null)                               // Заканчиваем цикл
                    {
                        zer = 12000;                                // Ставим макс.число, чтобы цикл закончился
                    }

                    if (zer < 12000)
                        massBase[zer] = zero.ToLower();
                }

                int t = 0;
                t = BezNuley(massBase);
                massBase2 = new string[t];                          // Новый массив, но уже без строк типа Null
                massBase2 = massBase;

                for (int probel = 0; probel < t; probel++)       // Если в конце пробел - убираем его
                {
                    if (massBase2[probel].Substring(massBase2[probel].Length - 1) == " ")
                    {
                        massBase2[probel] = massBase2[probel].Remove(massBase2[probel].Length - 1);
                    }
                }

                for (int i = 0; i < counterBase - 1; i++)
                {
                    

                    lstBase.Add(xlWorkSheetBase.get_Range(          // Заносим дату рождения в массив
                                                    comboBoxDateBirth.Text + (i + rowBirth).ToString(),
                                                    comboBoxDateBirth.Text +
                                                    (i + rowBirth).ToString()).Text);
                }

                txtBaseInfo.Text += "Данные загружены." + Environment.NewLine;      // Оповещение
                txtBaseInfo.Text += "Всего найдено: " + lstBase.Count.ToString() + " записей" + Environment.NewLine;
                xlWorkBookBase.Close(true, misValue, misValue);
                xlAppBase.Quit();                                   // Выход

                releaseObject(xlWorkSheetBase);
                releaseObject(xlWorkBookBase);
                releaseObject(xlAppBase);
            }
            else
            {
                MessageBox.Show("Выберите файл!");                  // Если не удалось открыть файл
            }
        }

        private int BezNuley(string[] m)                    // Функция, которая находит в массиве строки типа Null 
        {
            int ost = 0;
            for (int i = 0; i < m.Length; i++)
            {
                if (m[i] == null)
                {
                    ost = i;
                    i = m.Length;                   // Заканчиваем цикл, т.к. найдено количество чисел
                    return ost;
                }
            }
            return ost;
        }
        
        private void btnOpenBase_Click(object sender, EventArgs e)
        {
            OpenFileDialog openExcelBase = new OpenFileDialog();
            openExcelBase.Filter = "Excel 97-2003 (*.xls)|*.xls|Excel 2007 (*.xlsx)|*.xlsx";

            if (openExcelBase.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    txtPathBase.Text = openExcelBase.FileName;

                }
                catch (Exception ex)
                {
                    MessageBox.Show("Ошибка при открытии файла: " + ex.Message);
                }
            }
        }

        string[] checkedMassCopy = new string[12000];
        string[] ne_sovpadenie = new string[12000];
        List<string> lstDate = new List<string>();
        List<string> lstDate_nesovp = new List<string>();

        private void btnCheck_Click(object sender, EventArgs e)
        {
            string[] checkedMass = new string[12000];
            int counter = 0;
            int counter2 = 0;

            for (int z = 0; z < lst.Count; z++)
                for (int z2 = 0; z2 < massBase2.Length; z2++)
                {
                    if (lst[z].ToLower() == massBase2[z2])
                        if (massDate[z].ToString() == lstBase[z2])
                        {
                            checkedMass[counter] = mass[z];         // Заносим в массив совпавших
                            counter++;
                            lstDate.Add(massDate[z]);               // Заносим даты рождения в массив lstDate
                            z2 = massBase2.Length;
                        }

                    if (z2 == massBase2.Length - 1)                  // Если в базе нет совпадения,
                    {                                               // Заносим в массив ne_sovpadenie
                        ne_sovpadenie[counter2] = mass[z];
                        counter2++;
                        lstDate_nesovp.Add(massDate[z]);
                    }
                }

            checkedMassCopy = checkedMass;                          // Копируем совпавших в другой
                                                                    // массив checkedMassCopy
            MessageBox.Show("Найдено точных совпадений: " + counter.ToString());
        }

        private double Percentage(string s1, string s2)
        {
            int perc = 0;
            double percent1 = 0;
            for (int i = 0; i < s1.Length; i++)
                for (int j = 0; j < s2.Length; j++)
                {
                    if (j < i)
                        j = i;

                    if (i + 2 < j)
                    {
                        if (percent1 > 12)
                        {
                            j = s2.Length;
                        }
                        else
                            return 0;
                    }

                    if (j < s2.Length)
                        if (s1[i].ToString().ToLower() == s2[j].ToString().ToLower())
                        {
                            perc++;
                            percent1 = (perc * 100 / s1.Length);
                            j = s2.Length;
                        }
                }

            percent1 = (perc * 100 / s1.Length);
            return percent1;
        }

        private void btnSaveToExcel_Click(object sender, EventArgs e)               // Функция для отправления 
        {                                                                           // данных в Excel-файл
            workbook = excelapp.Workbooks.Add(System.Reflection.Missing.Value);     // Книга
            worksheet = workbook.Sheets[1];                                         // Лист 1
            try
            {
                worksheet.Cells[1, 2] = "Совпали";              // Первая строка, второй столбец "Совпали"
                worksheet.Cells[2, 2] = "ФИО";
                worksheet.Cells[2, 3] = "Дата рождения";
                worksheet.Cells[1, 4] = "Не совпали";
                worksheet.Cells[2, 4] = "ФИО";
                if (radioButton_dateBirth.Checked == true)
                    worksheet.Cells[2, 5] = "Дата рождения";
                if (radioButton_IIN.Checked == true)
                    worksheet.Cells[2, 5] = "ИИН";
                //worksheet.Cells.Font.Bold = true;
                worksheet.Columns[1].ColumnWidth = 5.7;         // Ширина первого столбца
                worksheet.Columns[2].ColumnWidth = 36.7;
                worksheet.Columns[3].ColumnWidth = 14.43;
                worksheet.Columns[4].ColumnWidth = 36.7;
                worksheet.Columns[5].ColumnWidth = 14.43;
                Excel.Range rng = excelapp.ActiveCell;
                rng.get_Characters(1, 52).Font.Bold = true;

                Excel.Range cells;

                if (radioButton_dateBirth.Checked == true)
                    if (massDate == null)
                    {
                        MessageBox.Show("Не загружен участок", "Предупреждение");
                        return;
                    }

                for (int i = 0; i < checkedMassCopy.Length; i++)    // Те, которые совпали
                {
                    if (checkedMassCopy[i] != null)
                    {
                        cells = worksheet.Range["A" + (i + 3).ToString(), "A" + (i + 3).ToString()];
                        cells.Value = i + 1;
                        cells = worksheet.Range["B" + (i + 3).ToString(), "B" + (i + 3).ToString()];
                        cells.Value = checkedMassCopy[i];
                        cells = worksheet.Range["C" + (i + 3).ToString(), "C" + (i + 3).ToString()];
                        cells.Value = lstDate[i];
                    }
                }

                for (int j = 0; j < ne_sovpadenie.Length; j++)      // Те, которые не совпали
                    if (ne_sovpadenie[j] != null)
                    {
                        cells = worksheet.Range["D" + (j + 3).ToString(), "D" + (j + 3).ToString()];
                        cells.Value = ne_sovpadenie[j];
                        cells = worksheet.Range["E" + (j + 3).ToString(), "E" + (j + 3).ToString()];
                        cells.Value = lstDate_nesovp[j];
                    }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ошибка: " + ex.Message);
            }
            excelapp.Visible = true;                                // Делаем выдимым Excel
            excelapp.UserControl = true;
        }

        private void checkFIO_CheckedChanged(object sender, EventArgs e)
        {
            int a = 0;
            if (checkFIO.Checked == true & checkBirth.Checked == true & checkIIN.Checked == true)
                a = 3;
            if (checkFIO.Checked == true & checkBirth.Checked == true & checkIIN.Checked == false)
                a = 2;
            if (checkFIO.Checked == true & checkBirth.Checked == false & checkIIN.Checked == true)
                a = 4;
            if (checkFIO.Checked == false & checkBirth.Checked == true & checkIIN.Checked == true)
                a = 5;
            if (checkFIO.Checked == false & checkBirth.Checked == true & checkIIN.Checked == false)
                a = 6;
            if (checkFIO.Checked == true & checkBirth.Checked == false & checkIIN.Checked == false)
                a = 1;
            if (checkFIO.Checked == false & checkBirth.Checked == false & checkIIN.Checked == true)
                a = 8;
            if (checkFIO.Checked == false & checkBirth.Checked == false & checkIIN.Checked == false)
                a = 7;

            CheckedBox(a);
        }

        private void CheckedBox(int cb)
        {
            switch (cb)
            {
                case 1:
                    comboBoxFIO.Visible = true;
                    comboBoxFIO2.Visible = true;
                    comboBoxDateBirth.Visible = false;
                    comboBoxDateBirth2.Visible = false;
                    comboBoxIIN.Visible = false;
                    comboBoxIIN2.Visible = false;
                    break;
                case 2:
                    comboBoxFIO.Visible = true;
                    comboBoxFIO2.Visible = true;
                    comboBoxDateBirth.Visible = true;
                    comboBoxDateBirth2.Visible = true;
                    comboBoxIIN.Visible = false;
                    comboBoxIIN2.Visible = false;
                    break;
                case 3:
                    comboBoxFIO.Visible = true;
                    comboBoxFIO2.Visible = true;
                    comboBoxDateBirth.Visible = true;
                    comboBoxDateBirth2.Visible = true;
                    comboBoxIIN.Visible = true;
                    comboBoxIIN2.Visible = true;
                    break;
                case 4:
                    comboBoxFIO.Visible = true;
                    comboBoxFIO2.Visible = true;
                    comboBoxDateBirth.Visible = false;
                    comboBoxDateBirth2.Visible = false;
                    comboBoxIIN.Visible = true;
                    comboBoxIIN2.Visible = true;
                    break;
                case 5:
                    comboBoxFIO.Visible = false;
                    comboBoxFIO2.Visible = false;
                    comboBoxDateBirth.Visible = true;
                    comboBoxDateBirth2.Visible = true;
                    comboBoxIIN.Visible = true;
                    comboBoxIIN2.Visible = true;
                    break;
                case 6:
                    comboBoxFIO.Visible = false;
                    comboBoxFIO2.Visible = false;
                    comboBoxDateBirth.Visible = true;
                    comboBoxDateBirth2.Visible = true;
                    comboBoxIIN.Visible = false;
                    comboBoxIIN2.Visible = false;
                    break;
                case 7:
                    MessageBox.Show("Нужно что-то выбрать!", "Предупреждение");
                    checkFIO.Checked = true;
                    comboBoxFIO.Visible = true;
                    comboBoxFIO2.Visible = true;
                    comboBoxDateBirth.Visible = false;
                    comboBoxDateBirth2.Visible = false;
                    comboBoxIIN.Visible = false;
                    comboBoxIIN2.Visible = false;
                    break;
                case 8:
                    comboBoxFIO.Visible = false;
                    comboBoxFIO2.Visible = false;
                    comboBoxDateBirth.Visible = false;
                    comboBoxDateBirth2.Visible = false;
                    comboBoxIIN.Visible = true;
                    comboBoxIIN2.Visible = true;
                    break;
            }
        }
        
        private void checkBirth_CheckedChanged(object sender, EventArgs e)
        {
            int b = 0;
            if (checkFIO.Checked == true & checkBirth.Checked == true & checkIIN.Checked == true)
                b = 3;
            if (checkFIO.Checked == true & checkBirth.Checked == true & checkIIN.Checked == false)
                b = 2;
            if (checkFIO.Checked == true & checkBirth.Checked == false & checkIIN.Checked == true)
                b = 4;
            if (checkFIO.Checked == false & checkBirth.Checked == true & checkIIN.Checked == true)
                b = 5;
            if (checkFIO.Checked == false & checkBirth.Checked == true & checkIIN.Checked == false)
                b = 6;
            if (checkFIO.Checked == true & checkBirth.Checked == false & checkIIN.Checked == false)
                b = 1;
            if (checkFIO.Checked == false & checkBirth.Checked == false & checkIIN.Checked == true)
                b = 8;
            if (checkFIO.Checked == false & checkBirth.Checked == false & checkIIN.Checked == false)
                b = 7;

            CheckedBox(b);
        }

        private void checkIIN_CheckedChanged(object sender, EventArgs e)
        {
            int c = 0;
            if (checkFIO.Checked == true & checkBirth.Checked == true & checkIIN.Checked == true)
                c = 3;
            if (checkFIO.Checked == true & checkBirth.Checked == true & checkIIN.Checked == false)
                c = 2;
            if (checkIIN.Checked == true & checkBirth.Checked == false & checkIIN.Checked == true)
                c = 4;
            if (checkFIO.Checked == false & checkBirth.Checked == true & checkIIN.Checked == true)
                c = 5;
            if (checkFIO.Checked == false & checkBirth.Checked == true & checkIIN.Checked == false)
                c = 6;
            if (checkFIO.Checked == true & checkBirth.Checked == false & checkIIN.Checked == false)
                c = 1;
            if (checkFIO.Checked == false & checkBirth.Checked == false & checkIIN.Checked == true)
                c = 8;
            if (checkFIO.Checked == false & checkBirth.Checked == false & checkIIN.Checked == false)
                c = 7;

            CheckedBox(c);
        }

        private void radioButton_dateBirth_CheckedChanged(object sender, EventArgs e)
        {
            lbl_DateBirth.Text = "Д/р";
        }

        private void radioButton_IIN_CheckedChanged(object sender, EventArgs e)
        {
            lbl_DateBirth.Text = "ИИН";
        }
    }
}
